
import { Button, Modal, Table} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect, useState } from 'react';
import { addLivre, deleteLivres, fetchLivres } from '@/livre-servives';
import { ILivre } from '@/entities';
import ITemLivre from '@/components/ITemLivre';
import FormLivre from '@/components/FormLivre';

export default function Home() {
const [livres, setLivres]=useState<ILivre[]>([]);

//  Debut pour afficher et selctionner un livre
const [show, setShow] = useState(false);
const [selectedLivre, setSelectedLivre] = useState<ILivre | null>(null);


// ces fonctions ici servent à afficher ou pas le modal et selectionner ppour afficher un livre
const handleClose = () => setShow(false);

const handleShow = (id: number) => {
  const livre = livres.find(livre => livre.id === id);

  // Pour eviter l'erreur sur setSelectedLivre(livre), je met if car "setSelectedLivre" ne sera appelé que si "livre" est défini
  if (livre) 
  setSelectedLivre(livre);
  setShow(true);
};

//  Fin pour afficher et selctionner un livre

// recuperer les livres
  useEffect(()=>{
    fetchLivres().then(data=>{
      setLivres(data);
    })
  
  }, [])


  // supprimer un livre
async function remove(id:any){
  await deleteLivres(id);
  setLivres(livres.filter(livre => livre.id !== id));
}

/*Code pour ajout du livre
En gros faites d'bord votre service et puis l'appeler ici c'est la fonction addLivre()
Notre fonction asynchrone va prendre un parametre livre de type ILivre (interface).
L'instruction await suspendra l'exécution de la fonction jusqu'à ce que la promesse renvoyée par addLivre soit résolue. ici la valeur est est "livre"
*/
async function ajoutLivre(livre:ILivre) {
  const ajout = await addLivre(livre);
  //ici on met à jour le tableau livres, donc contiendra tous les livres et les nouveaux element qui sont des "ajout"
  if (ajout) {
    setLivres([...livres, ajout]);
  }
}

// Afficher un livre avec un modal



  return (
    <div className='container'>
     
     <h2>Ma petite biblio</h2>
     <h3>Liste des livres</h3>

     {/* Notre FormLivre va prendre onSubmit comme props ce quipermet de faire passer la fonction ajoutLivre dans le copoment FormLivre */}
     <FormLivre onSubmitLivre={ajoutLivre} />

     <Table striped bordered hover>
      <thead>
        <tr>
          <th>Id</th>
          <th>Titre</th>
          <th>Auteur</th>
          <th>Dispo</th>
          <th colSpan={3} className="text-center">Actions</th>
        </tr>
      </thead>
      <tbody>
          {livres.map(livre =>
           <ITemLivre key={livre.id} livre={livre} onDelete={remove} onItem={() => handleShow(livre.id!)} />
          )}
  
       
      </tbody>
    </Table>

    {/* Le modal pour afficher un livre  */}
    <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{selectedLivre?.id}- {selectedLivre?.titre}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{selectedLivre?.auteur}</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleClose}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>

    </div>
  )
}
