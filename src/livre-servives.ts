import axios  from "axios";
import { ILivre } from "./entities";

export async function fetchLivres(){
    const response = await axios.get<ILivre[]>('http://localhost:8000/api/livre');
    return response.data;
}

export async function deleteLivres(id:any){
    await axios.delete('http://localhost:8000/api/livre/'+id);
}
export async function addLivre(livre:ILivre) {
    try {
        const response = await axios.post<ILivre>('http://localhost:8000/api/livre', livre);
        return response.data;
    } catch (error) {
    
        console.log(error);
      
    }
   
}
