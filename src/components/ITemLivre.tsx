
import { ILivre } from '@/entities';
import { Button} from 'react-bootstrap';

interface Props{
    livre:ILivre;
    onDelete: (id:any)=> void;
    onItem: (id:any) => void;
}

export default function ITemLivre({livre, onDelete, onItem}:Props ){

    const handleDelete = ()=>{
        onDelete(livre.id);
    }


  return (
    <>
        <tr>
            <td>{livre.id}</td>
            <td>{livre.titre}</td>
            <td>{livre.auteur}</td>
            <td>{livre.dispo}</td>
          {/*   selectionner un livre et l'afficher par le modal */}
            <td><Button variant="info" onClick={() => onItem(livre.id)}>Voir</Button></td>
            <td> <Button variant="warning">Modifier</Button></td>
            <td> <Button variant="danger" onClick={handleDelete}>Supprimer</Button></td>  
              </tr>
    </>
  )
}
