import { ILivre } from "@/entities";
import { useForm } from "react-hook-form";
import { Form, Button } from "react-bootstrap";


// on a notre interface qui est definit, on met le onSubmit qui se trouve dans notre index, elle respresente la fonction ajoutLivre. Elle renvoie une promesse de type void
// donc cet interface permet de faire  passer onSubmit comme etant un parametre dans notre fonction FormLivre
interface Props {
  onSubmitLivre: (livre: ILivre) => Promise<void>;
}


export default function FormLivre(props: Props) {

/*   Nous utilsons React Hook Form pour gerer notre formulaire, nous utilsons le hook useform pour initialiser le formulaire et retourner plusieurs propriétes
Nous avons register qui permet d'enregistrer chaque champs lors de la saison
handleSubmit lui gere la soumission et la transmission des données,
FormState (etat du formaulaire) permet de gerer les erreurs, ici on utilse la propriété errors
et reset qui va vider les champs apres soumisson du formulaire
 */
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<ILivre>();

  /*
  nous creons la fonction submit qui prends en parametre la data qui est ici les données saisies dans les champs du formulaires
  ensuite on apelle la fonctio props.onSubmitLivre( qu'on avait defini dans l'index et qui est ici la fonction ajoutLivre).
   */
  const onSubmit = (data: ILivre) => {
    props.onSubmitLivre(data);
    reset();
  };

  return (
    <>
    {/* ici nous utilisons handleSubmit pour valider le forumaire et transferer les données, il prend en parametre la fonction onSubmit qui lui sera appélé lors de la soumisson  */}

      <Form onSubmit={handleSubmit(onSubmit)}>
        <div className="row row-cols-2 row-cols-lg-5 g-2 g-lg-3">
          <div className="col">
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              {/* Dans notre input nous rajouter register pour enregister la données et ici le "name" du champ est titre, nous n'avons plus besoin ici de mettre onChange ou le name le useForm facilite cette gestion  */}
              <Form.Control
                type="text"
                placeholder="Titre"
                {...register("titre")}
              />
              <Form.Text className="text-muted">
                Well never share your email with anyone else.
              </Form.Text>
            </Form.Group>
          </div>
          <div className="col">
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Auteur</Form.Label>
              <Form.Control
                type="text"
                placeholder="Auteur"
                {...register("auteur")}
              />
            </Form.Group>
          </div>

          <div className="col">
            <Form.Group className="mb-3" controlId="formBasicCheckbox">
              <Form.Label>Disponibilté</Form.Label>
              <Form.Control
                type="number"
                placeholder="0"
                {...register("dispo", {
                  valueAsNumber: true,
                  required: true,
                  min: 0,
                  max: 1,
                })}
              />
              {errors.dispo?.type === "required" && (
                <p role="alert">Entre 0 et 1</p>
              )}
            </Form.Group>
          </div>
          <div className="col">
            <Button variant="primary" type="submit">
              Ajouter
            </Button>
          </div>
        </div>
      </Form>
    </>
  );
}
